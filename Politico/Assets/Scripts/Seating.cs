﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seating : MonoBehaviour
{
    const float goldenRation = 1.62f;

    int Rows
    {
        get
        {
            int currentRowCount = 1;
            float circumSum = (2 * Mathf.PI * RadiusAtRow(currentRowCount)) * (semiCircleAngle/360f);
            while (!SeatsFitInCirc(circumSum, seats))
            {
                currentRowCount++;
                circumSum += (2 * Mathf.PI * RadiusAtRow(currentRowCount)) * (semiCircleAngle / 360f);
            }
            return currentRowCount;
        }
    }
    int[] RowSeatCounts
    {
        get
        {
            int seatsLeft = seats;
            int rows = Rows;
            List<int> rsc = new List<int>();
            float rowRadiusTotal = 0;
            for (int i = 0; i < rows; i++)
            {
                rowRadiusTotal += RadiusAtRow(i + 1);
            }
            for (int i = 0; i < rows; i++)
            {
                if (i == Rows - 1) rsc.Add(seatsLeft);
                else
                {
                    rsc.Add(Mathf.FloorToInt((RadiusAtRow(i + 1) / rowRadiusTotal)*seats));
                    seatsLeft -= rsc[i];
                }
            }
            return rsc.ToArray();
        }
    }
    public float seatBuffer = 2f;
    public GameObject prefab;
    public int seats = 20;
    [Range(30f,180f)] public float semiCircleAngle = 180f;
    public float baseRadius = 5f;
    public float rowSpacing = 2f;
    public float rowHeightStep = 1f;
    public int rowsPerStep = 1;

    public float RowRatio
    {
        get
        {
            return (baseRadius + rowSpacing) / baseRadius;
        }
    }
    public float RadiusAtRow(int row)
    {
        float rad = baseRadius;
        for (int i = 1; i < row; i++)
        {
            rad += rowSpacing;
        }
        return rad;
    }
    public float StepAtRow(int row)
    {
        float hei = 0f;
        for (int i = 1; i < row; i++)
        {
            if(i % rowsPerStep == 0 && i != 0)hei += rowHeightStep;
        }
        return hei;
    }


    private void Start()
    {
        startingRotation = transform.rotation.eulerAngles;
    }

    protected bool SeatsFitInCirc(float circa, int seatos)
    {
        if (circa > (seatos - 1) * seatBuffer) return true;
        return false;
    }


    protected Vector3 startingRotation;
    protected List<GameObject> seatPositions = new List<GameObject>(); 
    void SpawnMotherfuckingCircle()
    {
        transform.rotation = Quaternion.Euler(startingRotation);
        foreach(GameObject seatpos in seatPositions)
        {
            DestroyImmediate(seatpos);
        }
        seatPositions.Clear();

        for (int i = 0; i < Rows; i++)
        {
            for (int j = 0; j < RowSeatCounts[i]; j++)
            {
                float angle = ((((Mathf.PI * 2) * (semiCircleAngle / 360f)) / RowSeatCounts[i]) / 2f) + ((j * (Mathf.PI * 2) * (semiCircleAngle / 360f)) / RowSeatCounts[i]);
                float x = Mathf.Cos(angle) * RadiusAtRow(i + 1);
                float z = Mathf.Sin(angle) * RadiusAtRow(i + 1);
                Vector3 pos = transform.position + new Vector3(x, StepAtRow(i + 1), z);
                float angleDegrees = -angle * Mathf.Rad2Deg;
                Quaternion rot = Quaternion.Euler(0, angleDegrees, 0);
                seatPositions.Add(Instantiate(prefab, pos, rot, transform));
            }
        }

        transform.Rotate(Vector3.up, -(180f - semiCircleAngle)/2);
    }

    float prevAngle = -1f;
    private void Update()
    {
        if(prevAngle != semiCircleAngle)
        {
            prevAngle = semiCircleAngle;
            SpawnMotherfuckingCircle();
        }
    }
}
